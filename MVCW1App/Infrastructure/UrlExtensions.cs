﻿using Microsoft.AspNetCore.Http;

namespace MVCW1App.Infrastructure
{
    public static class UrlExtensions
    {
        public static string PatgAndQuery(this HttpRequest request) => request.QueryString.HasValue ? $"{request.Path}{request.QueryString}" : request.Path.ToString();
    }
}
