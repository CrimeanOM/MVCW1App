﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using MVCW1App.Models;
using Microsoft.EntityFrameworkCore;

namespace MVCW1App.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private AppDbContext context;
        public NavigationMenuViewComponent(AppDbContext ctx)
        {
            context = ctx;
        }
        public IViewComponentResult Invoke()
        {
            var Products = context.Products.FromSql("GetAllProducts").ToArray();

            ViewBag.SelectedCategory = RouteData?.Values["category"];
            return View(Products
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x => x));
        }
    }
}
