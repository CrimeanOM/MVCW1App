﻿using Microsoft.AspNetCore.Mvc;
using MVCW1App.Models;

namespace MVCW1App.Components
{
    public class CartSummaryViewComponent : ViewComponent
    {
        private Cart cart;
        public CartSummaryViewComponent(Cart cartService)
        {
            cart = cartService;
        }
        public IViewComponentResult Invoke() => View(cart);
    }
}
