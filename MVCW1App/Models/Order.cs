﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MVCW1App.Models
{
    public class Order
    {
        [BindNever]
        public int OrderId { get; set; }

        public ICollection<CartLine> Lines { get; set; }

        public string WMI_MERCHANT_ID { get; set; }

        public string WMI_PAYMENT_AMOUNT { get; set; }

        public string WMI_CURRENCY_ID { get; set; }

        [Required(ErrorMessage = "Please, enter order description")]
        public string WMI_DESCRIPTION { get; set; }

        public string WMI_SUCCESS_URL { get; set; }

        public string WMI_FAIL_URL { get; set; }
    }
}
