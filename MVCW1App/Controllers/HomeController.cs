﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVCW1App.Models;
using MVCW1App.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace MVCW1App.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppDbContext context;

        public int PageSize = 4;

        public HomeController(AppDbContext ctx)
        {
            context = ctx;
        }

        public IActionResult Index() => View();

        public ViewResult Products(string category, int page = 1)
        {            
            var Products = context.Products.FromSql("GetAllProducts").ToArray();
            
            return View(new ProductsViewModel
            {
                Products = Products
               .Where(p => category == null || p.Category == category)
               .OrderBy(p => p.ProductId)
               .Skip((page - 1) * PageSize)
               .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = category == null ? Products.Count() : Products.Where(e => e.Category == category).Count()
                },
                CurrentCategory = category
            });
        }
      
        public IActionResult Contacts() => View();


        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
