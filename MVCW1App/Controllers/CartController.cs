﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCW1App.Infrastructure;
using MVCW1App.Models;
using MVCW1App.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace MVCW1App.Controllers
{
    public class CartController : Controller
    {
        private AppDbContext context;
        private Cart cart;
        public CartController(AppDbContext ctx, Cart cartService)
        {
            context = ctx;
            cart = cartService;
        }
        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = cart,
                ReturnUrl = returnUrl
            });
        }
        public RedirectToActionResult AddToCart(int productId, string returnUrl)
        {
            System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@productId", $"{productId}");
            var product = context.Products.FromSql("GetProduct @productId", param1).ToList().FirstOrDefault();
            if (product != null)
                cart.AddItem(product, 1);
            
            return RedirectToAction("Index", new { returnUrl });
        }
        public RedirectToActionResult RemoveFromCart(int productId, string returnUrl)
        {
            System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@productId", $"{productId}");
            var product = context.Products.FromSql("GetProduct @productId", param1).ToList().FirstOrDefault();
            if (product != null)
                cart.RemoveLine(product);
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}
