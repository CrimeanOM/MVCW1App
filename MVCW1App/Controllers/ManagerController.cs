﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVCW1App.Models;

namespace MVCW1App.Controllers
{
    public class ManagerController : Controller
    {
        private readonly AppDbContext context;
        public ManagerController(AppDbContext ctx)
        {
            context = ctx;
        }
        public IActionResult Index()
        {
            var products = context.Products.FromSql("GetAllProducts").ToArray();
            return View(products);
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Product product)
        {
            System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@category", $"{product.Category}");
            System.Data.SqlClient.SqlParameter param2 = new System.Data.SqlClient.SqlParameter("@description", $"{product.Description}");
            System.Data.SqlClient.SqlParameter param3 = new System.Data.SqlClient.SqlParameter("@name", $"{product.Name}");
            System.Data.SqlClient.SqlParameter param4 = new System.Data.SqlClient.SqlParameter("@price", $"{product.Price}");
            context.Database.ExecuteSqlCommand("InsertIntoProducts @category, @description, @name, @price", param1, param2, param3, param4);
            await context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Edit(int? productId)
        {
           if (productId != null)
            {
                System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@productId", $"{productId}");
                var product = context.Products.FromSql("GetProduct @productId", param1).ToList().FirstOrDefault();
                if (product != null)
                    return View(product);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Product product)
        {
            System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@productId ", $"{product.ProductId}");
            System.Data.SqlClient.SqlParameter param2 = new System.Data.SqlClient.SqlParameter("@category", $"{product.Category}");
            System.Data.SqlClient.SqlParameter param3 = new System.Data.SqlClient.SqlParameter("@description", $"{product.Description}");
            System.Data.SqlClient.SqlParameter param4 = new System.Data.SqlClient.SqlParameter("@name", $"{product.Name}");
            System.Data.SqlClient.SqlParameter param5 = new System.Data.SqlClient.SqlParameter("@price", $"{product.Price}");
            context.Database.ExecuteSqlCommand("EditProduct @productId, @category, @description, @name, @price", param1, param2, param3, param4, param5);
            await context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int? productId)
        {
            if (productId != null)
            {
                System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@productId", $"{productId}");
                var product = context.Products.FromSql("GetProduct @productId", param1).ToList().FirstOrDefault();
                if (product != null)
                {
                    context.Database.ExecuteSqlCommand("RemoveProduct @productId", param1);
                    await context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            return NotFound();
        }
    }
}
