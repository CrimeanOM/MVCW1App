﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net;
using MVCW1App.Models;

namespace MVCW1App.Controllers
{
    public class OrderController : Controller
    {
        private AppDbContext context;
        private Cart cart;

        public OrderController(AppDbContext ctx, Cart cartService)
        {
            context = ctx;
            cart = cartService;
        }
        public ViewResult Checkout() => View(new Order { });

        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            if (cart.Lines.Count() == 0)
                ModelState.AddModelError("", "Your cart is empty!");
            if (ModelState.IsValid)
            {
                order.Lines = cart.Lines.ToList();
                context.AttachRange(order.Lines.Select(l => l.Product));
                if (order.OrderId == 0)
                {
                    System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@WMI_CURRENCY_ID", $"{order.WMI_CURRENCY_ID}");
                    System.Data.SqlClient.SqlParameter param2 = new System.Data.SqlClient.SqlParameter("@WMI_DESCRIPTION", $"{order.WMI_DESCRIPTION}");
                    System.Data.SqlClient.SqlParameter param3 = new System.Data.SqlClient.SqlParameter("@WMI_FAIL_URL", $"{order.WMI_FAIL_URL}");
                    System.Data.SqlClient.SqlParameter param4 = new System.Data.SqlClient.SqlParameter("@WMI_MERCHANT_ID", $"{order.WMI_MERCHANT_ID}");
                    System.Data.SqlClient.SqlParameter param5 = new System.Data.SqlClient.SqlParameter("@WMI_PAYMENT_AMOUNT", $"{order.WMI_PAYMENT_AMOUNT}");
                    System.Data.SqlClient.SqlParameter param6 = new System.Data.SqlClient.SqlParameter("@WMI_SUCCESS_URL", $"{order.WMI_SUCCESS_URL}");

                    context.Database.ExecuteSqlCommand("InsertIntoOrders @WMI_CURRENCY_ID, @WMI_DESCRIPTION, @WMI_FAIL_URL, @WMI_MERCHANT_ID, @WMI_PAYMENT_AMOUNT, @WMI_SUCCESS_URL", param1, param2, param3, param4, param5, param6);
                    context.SaveChanges();
                }         

                return RedirectToAction(nameof(Completed), order);
            }
            else
                return View(order);
        }
        public ViewResult Completed(Order order)
        {
            order.WMI_MERCHANT_ID = "112745980867";
            order.WMI_CURRENCY_ID = "840";
            order.WMI_PAYMENT_AMOUNT = cart.Lines.Sum(c => c.Product.Price * c.Quantity).ToString();
            order.Lines = cart.Lines.ToArray();
            return View(order);
        }

        public ViewResult Success()
        {
            cart.Clear();
            return View();
        }
        public ViewResult Fail() => View();
    }
}
